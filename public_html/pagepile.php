<?PHP

$oauth_pagepile = (object) array ( 'is_authorized' => false ) ;
if ( get_current_user() == 'tools.pagepile' ) {
	require_once ( '/data/project/pagepile/public_html/php/oauth.php' ) ;
	$oa = new MW_OAuth ( 'pagepile' , 'www' , 'wikimedia' ) ;
	$oauth_pagepile = $oa->doIdentify() ;
	unset ( $oa ) ;
}

/**
The PagePile class.
*/
class PagePile {

	/// Error string, for recoverable errors.
	public $error ;

	public $output_limit = 0 ;
	public $output_sort = 'ns_title' ;

	protected $mysql_server = 'tools-db' ;
	protected $dbname = 's51211__pagepile_p' ;
	protected $file_root = '/data/project/shared/pagepile' ;
	protected $defaultBatchSize = 10000 ;
	
	protected $db ;
	protected $sqlite_file ;
	protected $sqlite ;
	protected $wiki ;
	protected $namespace_cache ;
	protected $pile_id ;
	protected $transaction_is_running ;

	/// List of tools that can import pagepiles, with template URL
	public $consumers = array (
		'Filters' => [ 'https://tools.wmflabs.org/pagepile/?menu=filter&pile=$1' ] ,
		'PetScan' => [ 'https://petscan.wmflabs.org' , 'Successor to CatScan2, QuickIntersection, AutoList etc.' ] ,
		'AutoList 2' => [ 'https://tools.wmflabs.org/autolist/index.php?pagepile=$1' ] ,
		'FIST' => [ 'https://tools.wmflabs.org/fist/fist.php?datatype=pagepile&data=$1' , 'Find free images on Wikipedia, Commons, Flicker, etc.' ] ,
		'WD-FIST' => [ 'https://tools.wmflabs.org/fist/wdfist/index.html?pagepile=$1' , 'Add images to Wikidata items' ] ,
		'Not in the other language' => [ 'https://tools.wmflabs.org/not-in-the-other-language/?pagepile=$1' ] ,
		'Get item names' => [ 'https://tools.wmflabs.org/wikidata-todo/get_item_names.php?pagepile=$1' , 'Get Wikidata item labels in various formats and languages' ] ,
		'Linked items' => [ 'https://tools.wmflabs.org/wikidata-todo/linked_items.php?pagepile=$1' ] ,
		'Item properties' => [ 'https://tools.wmflabs.org/wikidata-todo/tabernacle.html?pagepile=$1' , 'View and download item properties as a table with the Tabernacle tool' ] ,
		'View on map' => [ 'http://www.mappingsupport.com/p/gmap4.php?q=https://tools.wmflabs.org/wikidata-todo/pagepile2kml.php?pagepile=$1' , 'View map with dynamically generated KML overlay' ] ,
		'Count page views' => [ 'https://tools.wmflabs.org/glamtools/treeviews/?q={%22pagepile%22:%22$1%22}' , 'Get page views for the pages in the pile on TreeViews' ] ,
		'GLAMorous' => [ 'https://tools.wmflabs.org/lp-tools/tmp/glamorous.php?pagepile=$1' , 'Track usage of Commons images on other projects' ] ,
		'GLAMorous 2' => [ 'http://tools.wmflabs.org/lp-tools/tmp/glamorous/?mode=pagepile&text=$1' , 'Track usage and views of Commons images on other projects' ] ,
	) ;
	
	// List of tools that can generate pagepiles
	public $generators = array (
		'PetScan' => 'https://petscan.wmflabs.org' ,
		'AutoList 2' => 'https://tools.wmflabs.org/autolist/index.php' ,
#		'CatScan 2' => 'https://tools.wmflabs.org/catscan2/catscan2.php' ,
#		'QuickIntersection' => 'https://tools.wmflabs.org/quick-intersection/index.php' ,
		'Not in the other language' => 'https://tools.wmflabs.org/not-in-the-other-language/' ,
		'Linked items' => 'https://tools.wmflabs.org/wikidata-todo/linked_items.php' ,
		'Wikidata Terminator' => 'https://tools.wmflabs.org/wikidata-terminator' ,
	) ;

	
	/** Constructor
		\param $seed Optional parameter to initialize from existing sqlite. Can be sqlite file path or numeric pagepile ID.
	*/
	function __construct ( $seed = '' ) {
		$this->namespace_cache = (object) array() ;
		
		if ( $seed != '' ) {
			if ( is_numeric ( $seed ) ) $this->openSqliteID ( $seed ) ;
			else $this->openSqliteFile ( $seed ) ;
		}
	}

	/// Descructor.
	function __destruct () {
		$this->reset() ;
	}
	
	/**
		Returns a MySQL DB handler for the main pagepile database.
		\return	MySQL DB handler object
	*/
	public function getDB() {
		$this->openDB() ;
		return $this->db ;
	}


	/**
		Returns a HTML link text to a specified page.
		\param $o Object of a sqlite row from the pages table, as passed by each()
		\return HTML link text.
	*/
	public function getPageLink ( $o ) {
		$page = $this->getFullPageTitle ( $o->page , $o->ns ) ;
		$name = str_replace ( '_' , ' ' , $page ) ;
		$server = $this->wiki2server ( $this->getWiki() ) ;
		$ret = "<a href='https://$server/wiki/" . urlencode($page) . "'>$name</a>" ;
		return $ret ;
	}
	
	/**
		Creates a full Wiki page title, with namespace prefix if appropriate
		\param $page page name, without prefix
		\param $ns namespace number (optional; default=0)
		\return Full page title
	*/
	public function getFullPageTitle ( $page , $ns = 0 ) {
		$wiki = $this->getWiki() ;
		if ( isset($this->namespace_cache->$wiki->id2name[$ns]) ) {
			if ( $this->namespace_cache->$wiki->id2name[$ns] == '' ) return $page ;
			else return $this->namespace_cache->$wiki->id2name[$ns] . ':' . $page ;
		} else {
			return $page ;
		}
	}

	public function getArray ( $offset = 0 , $limit = 999999999999 ) {
		return $this->getArrayWithMode ( $offset , $limit , 'array' ) ;
	}

	public function getPrefixedArray ( $offset = 0 , $limit = 999999999999 ) {
		return $this->getArrayWithMode ( $offset , $limit , 'prefixed' ) ;
	}
	
	/**
		Calls a PHP function for each page in the pile.
		Calls passed function with row as object in first parameter, PagePile object in second parameter.
		\param $anon (anonymous) PHP function.
	*/
	public function each ( $anon ) {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$sql = "SELECT * FROM pages ORDER BY " ;
		if ( $this->output_sort == 'random' ) $sql .= "RANDOM()" ;
		else $sql .= "ns,page" ; # Default: ns_title
		if ( $this->output_limit > 0 ) $sql .= " LIMIT " . $this->output_limit ;
		$results = $sqlite->query ( $sql ) ;
		while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
			$anon ( (object) $row , $this ) ;
		}
	}
	
	public function getAutoDesc () {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$sql = "SELECT * FROM meta WHERE `key`='track'" ;
		$results = $sqlite->query ( $sql ) ;
		$row = '' ;
		if ($row = $results->fetchArray(SQLITE3_ASSOC)) $row = (object) $row ;
		else return "Automatic description unavailable." ;
		$j = json_decode ( $row->value ) ;
		return $this->parseTrack ( $j ) ;
	}
	
	public function getLanguage () {
		$server = $this->wiki2server ( $this->getWiki() ) ;
		if ( !preg_match ( '/^([^.]+)\.([^.]+)\.org$/' , $server , $m ) ) return '' ;
		return $m[1] ;
	}

	public function getProject () {
		$server = $this->wiki2server ( $this->getWiki() ) ;
		if ( !preg_match ( '/^([^.]+)\.([^.]+)\.org$/' , $server , $m ) ) return '' ;
		return $m[2] ;
	}
	
	protected function parseTrack ( $j , $level = 0 ) {
		$ret = '' ;
		if ( $level == 0 ) $ret .= "<div style='margin:3px;padding:2px;border:5px solid #DDD'><div class='lead'>Automatic page pile history</div>" ;
		if ( is_array ( $j ) ) {
			$ret .= "<div style='border-top:1px solid black;margin-top:5px;margin-left:20px;'" ;
			foreach ( $j AS $sub ) {
				$ret .= "<div style='border:1px dotted #DDD;margin:2px;padding:2px'>" . $this->parseTrack ( $sub , $level+1 ) . "</div>" ;
			}
			$ret .= "</div>" ;
		} else if ( is_object ( $j ) ) {
			if ( isset($j->label) ) $ret .= "<div><b>" . $j->label . "</b></div>" ;
			if ( $j->action == 'wdq' ) $ret .= "<div><tt><a href='{$j->url}'>" . $j->wdq . "</a></tt></div>" ;
		} else {
			$ret .= $j ;
		}
		if ( $level == 0 ) $ret .= "</div>" ;
		return $ret ;
	}
	
	/**
		Filters a page pile by calling a PHP function for each page, 
		keeping the ones where the function returns true. See "each" for details.
		\param $anon (anonymous) PHP function
	*/
	public function filterKeep ( $anon ) {
		$this->doFilter ( $anon , true ) ;
	}
	
	/**
		Filters a page pile by calling a PHP function for each page, 
		removing the ones where the function returns true. See "each" for details.
		\param $anon (anonymous) PHP function
	*/
	public function filterRemove ( $anon ) {
		$this->doFilter ( $anon , false ) ;
	}
	
	/**
		Returns the number of pages in the current pile.
		\return number of pages in the list
	*/
	public function getPageNumber () {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		if ( !isset($sqlite) ) throw new Exception ( "SQLITE not ready" ) ;
		$sql = "select count(*) AS cnt FROM pages" ;
		return $this->sqlite->querySingle ( $sql ) * 1 ;
	}
	
	public function getPileID () {
		return $this->pile_id ;
	}
	
	public function getSqliteFile() {
		return $this->sqlite_file ;
	}
	
	protected function isBrowser () {
		if ( !isset($_SERVER['HTTP_USER_AGENT']) ) return false ;
		return preg_match ( '/[Mm]ozilla/' , $_SERVER['HTTP_USER_AGENT'] ) ;
	}
	
	public function printAndEnd ( $do_quit = true ) {
		$this->commitTransaction() ;
		$out = array ( 'id' => $this->getPileID() , 'file' => $this->getSqliteFile() ) ;
		
		$req = isset($_REQUEST['pagepile_format'])?$_REQUEST['pagepile_format']:'' ;
		if ( $this->isBrowser() and $req != 'json' ) {
		
			$full_page = !headers_sent() ;
			
			if ( $full_page ) {
				header('Content-Type: text/html');
				print "<!DOCTYPE html>\n<html><head><meta charset='utf-8' /><script src='https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/2.1.4/jquery.min.js'></script></head><body>" ;
			}

			$id = $out['id'] ;
			print "
<p style='font-size:20pt'>Your new <a href='/pagepile' target='_blank'>PagePile</a> ID is <b>$id</b>!</p>
<p>You can use this ID to perform subsequent actions on the results.</p>
<p>The pile contains " . $this->getPageNumber() . " pages.</p>
<p><a href='/pagepile/api.php?id=$id&action=get_data&format=html' target='_blank'>View the results</a>, or use it in " . $this->getConsumerHTML() . "</p>
<p style='font-size:8pt'>Programmers: To force JSON output, add <tt>pagepile_format=json</tt> to your GET/POST request.
You can also use <tt>callback=</tt> for JSONP.</p>" ;
			if ( $full_page ) print "</body></html>" ;
			
		} else {
			header('Content-type: application/json; charset=utf-8');
			$callback = '' ;
			if ( isset($_REQUEST['callback']) ) $callback = $_REQUEST['callback'] ;
			if ( $callback != '' ) print $callback . "(" ;
			print json_encode ( $out ) ;
			if ( $callback != '' ) print ")" ;
		}
		if ( $do_quit ) exit ( 0 ) ;
	}
	
	public function getSqlite() {
		if ( !isset($this->sqlite) ) {
			try {
				$this->sqlite = new SQLite3 ( $this->getSqliteFile() ) ;
			} catch (Exception $e) {
				return null ;
			}
			if ( !isset($this->sqlite) ) throw new Exception ( "No sqlite database open. Use createNewPile()." ) ;
		}
		return $this->sqlite ;
	}
	
	public function openSqliteID ( $id ) {
		$id = preg_replace ( '/\D/' , '' , $id ) * 1 ;
		$this->openDB() ;
		$sql = "SELECT * FROM piles WHERE id=$id" ;
		if(!$result = $this->db->query($sql)) throw new Exception('1 There was an error running the query [' . $this->db->error . ']');
		if($o = $result->fetch_object()){
			$this->openSqliteFile ( $o->path ) ;
			$this->pile_id = $id ;
			return ;
		} else throw new Exception ( "No pile #$id!" ) ;
	}
	
	public function getWiki () {
		if ( !isset($this->wiki) ) {
			$sqlite = $this->getSqlite() ;
			if ( !isset($sqlite) ) throw new Exception('SQLITE not ready') ;
			$this->wiki = $sqlite->querySingle ( "SELECT `value` FROM meta WHERE `key`='wiki'" ) ;
		}
		return trim ( strtolower ( $this->wiki ) ) ;
	}
	
	protected function error ( $msg ) {
		$this->error = $msg ;
	}
	
	public function openSqliteFile ( $file ) {
		if ( !file_exists ( $file ) ) return $this->error ( "SQLITE file $file does not exist!" ) ;
		if ( filesize ( $file ) == 0 ) return $this->error ( "SQLITE file $file is empty!" ) ;
		$this->reset() ;
		$this->sqlite_file = $file ;
		$this->loadNamespaces ( $this->getWiki() ) ;
		$this->loadNamespaces ( 'enwiki' ) ; // Default
	}
	
	public function beginTransaction () {
		if ( $this->transaction_is_running ) return ;
		$sqlite = $this->getSqlite() ;
		if ( !isset($sqlite) ) return ;
		$this->transaction_is_running = true ;
		$sql = "BEGIN EXCLUSIVE TRANSACTION" ;
		$sqlite->exec ( $sql ) ;
	}

	public function commitTransaction () {
		if ( !$this->transaction_is_running ) return ;
		$this->transaction_is_running = false ;
		$sqlite = $this->getSqlite() ;
		if ( !isset($sqlite) ) return ;
		$sql = "COMMIT" ;
		$sqlite->exec ( $sql ) ;
	}
	
	
	/**
		Adds all pages (any namespace) on the current wiki that are linked from the passed $pages.
		\param $pages array of (namespace-prefixed) pages
		\param $mode can be 'any' (default) or 'all' (requiring all passed pages to link to the same target)
	*/
	public function addPagesLinkedFrom ( $pages , $mode = 'any' ) {
		if ( count ( $pages ) == 0 ) return ;
		$db = $this->openWikiDB() ;
		$query = array() ;
		foreach ( $pages AS $p ) {
			$parts = $this->splitFullPageName ( $this->mytrim ( $p ) ) ;
			$query[$parts[1]][] = $db->real_escape_string ( $parts[0] ) ;
		}
		
		foreach ( $query AS $ns => $v ) {
			$query[$ns] = "( page_namespace='$ns' AND page_title IN ('" . implode("','",$v) . "') )" ;
		}
		
		$sql = "select pl_namespace,pl_title,count(*) AS cnt from page,pagelinks where page_id=pl_from and ( " . implode ( ' OR ' , $query ) . " ) GROUP BY pl_namespace,pl_title" ;
		if ( $mode == 'all' ) $sql .= " HAVING cnt=" . count($pages) ;
		else if ( $mode != 'any' ) throw new Exception ( "Bad mode '$mode' for addPagesLinkedFrom" ) ;
		
		if(!$result = $db->query($sql)) throw new Exception ( 'There was an error running the query [' . $db->error . ']'."\n$sql\n\n" ) ;
		while($o = $result->fetch_object()){
			$this->addPage ( $o->pl_title , $o->pl_namespace ) ;
		}
		$this->commitTransaction() ;
	}
	
	
	/**
		Creates a new PagePile sqlite file
		\param $language Language (e.g. "de") or wiki (e.g. "dewiki")
		\param $project Project (e.g. "wikipedia") or blank ("") when using $language as a wiki
	*/
	public function createNewPile ( $language , $project = '' , $user = '' ) {
		$this->wiki = $language ;
		if ( $language != '' && $project != '' ) {
			$this->wiki = $this->lp2wiki ( $language , $project ) ;
		}
		
		// Create unique file name
		$file = $this->getNewSqliteFile($user) ;
		$this->initSqliteFile($file) ;
		$this->loadNamespaces ( $this->getWiki() ) ;
		$this->loadNamespaces ( 'enwiki' ) ; // Default
	}
	
	/**
		Creates a new PagePile sqlite file, but does NOT register it with "central".
		The file will be cleaned up when the object is destroyed. Careful with copying this object!
		\param $language Language (e.g. "de") or wiki (e.g. "dewiki")
		\param $project Project (e.g. "wikipedia") or blank ("") when using $language as a wiki
	*/
	public function createNewUnregisteredPile ( $language , $project = '' , $user = '' ) {
		$this->wiki = $language ;
		if ( $language != '' && $project != '' ) {
			$this->wiki = $this->lp2wiki ( $language , $project ) ;
		}
		
		// Create unique file name
		$file = $this->getNewSqliteFileDirect($user,'no') ;
		$this->initSqliteFile($file) ;
		$this->loadNamespaces ( $this->getWiki() ) ;
		$this->loadNamespaces ( 'enwiki' ) ; // Default
	}
	
	public function addPage ( $page , $ns = -999 , $json = array() ) {
		$this->beginTransaction() ;
		$sqlite = $this->getSqlite() ;
		if ( gettype($json) == 'object' or gettype($json) == 'array' ) $json = json_encode ( (object) $json ) ;
		$json = $sqlite->escapeString ( $json ) ;
		
		$page = trim ( str_replace ( '_' , ' ' , $page ) ) ;
		if ( $page == '' ) return ;
		
		if ( $ns == -999 ) {
			$a = $this->splitFullPageName ( $page ) ;
//print_r ( $a ) ;
			$page = $a[0] ;
			$ns = $a[1] ;
		} else {
			$page = str_replace ( ' ' , '_' , $page ) ;
		}
		$page = $sqlite->escapeString ( $page ) ;
		
		$ns *= 1 ;
		$sql = "INSERT OR IGNORE INTO pages (page,ns,json) VALUES ('$page',$ns,'$json')" ;
		$sqlite->exec ( $sql ) ;
	}
	
	public function getCurrentTimestamp () {
		return date('YmdHis') ;
	}
	
	/**
		Generates a new PagePile object with a new ID and sqlite file. The sqlite file is an identical copy of the current one.
		\return New PagePile object.
	*/
	public function duplicate () {
		$this->commitTransaction() ;
		$ret = new PagePile ;
		$ret->getNewSqliteFile () ;
		$source = $this->getSqliteFile() ;
		$target = $ret->getSqliteFile() ;
		if ( !copy ( $source , $target ) ) throw new Exception ( "Copy from $source to $target failed!" ) ;
		return $ret ;
	}
	
	/**
		Keeps only the pages that are also in another pagepile.
		\param $other_pile_candidate Numeris pile ID, or PagePile object
	*/
	public function subset ( $other_pile_candidate ) {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$other_pile = $this->getAsPile($other_pile_candidate) ;
		$this->ensureCompatability($other_pile) ;
		$other = $other_pile->getSqliteFile() ;
		if ( !isset($other) ) throw new Exception ( "No 'other' sqlite file to subset" ) ;
		$sql = array() ;
		$sql[] = "ATTACH DATABASE '" . $sqlite->escapeString($other) . "' AS 'other'" ;
		$sql[] = "BEGIN EXCLUSIVE TRANSACTION" ;
		$sql[] = "DELETE FROM main.pages WHERE NOT EXISTS (SELECT * FROM other.pages WHERE other.pages.page=main.pages.page AND other.pages.ns=main.pages.ns)" ;
		$sql[] = "COMMIT" ;
		$sql[] = "DETACH DATABASE 'other'" ;
		$sql = implode ( '; ' , $sql ) ; $sqlite->exec ( $sql ) ;
		$this->addTrack ( array ( 'action' => 'subset' , 'pile' => $other_pile->getTrack() , 'ts' => $this->getCurrentTimestamp() ) ) ;
	}
	
	/**
		Keeps only the pages that are not in another pagepile.
		\param $other_pile_candidate Numeris pile ID, or PagePile object
	*/
	public function exclusive ( $other_pile_candidate ) {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$other_pile = $this->getAsPile($other_pile_candidate) ;
		$this->ensureCompatability($other_pile) ;
		$other = $other_pile->getSqliteFile() ;
		if ( !isset($other) ) throw new Exception ( "No 'other' sqlite file to subset" ) ;
		$sql = array() ;
		$sql[] = "ATTACH DATABASE '" . $sqlite->escapeString($other) . "' AS 'other'" ;
		$sql[] = "BEGIN EXCLUSIVE TRANSACTION" ;
		$sql[] = "DELETE FROM main.pages WHERE EXISTS (SELECT * FROM other.pages WHERE other.pages.page=main.pages.page AND other.pages.ns=main.pages.ns)" ;
		$sql[] = "COMMIT" ;
		$sql[] = "DETACH DATABASE 'other'" ;
		$sql = implode ( '; ' , $sql ) ; $sqlite->exec ( $sql ) ;
		$this->addTrack ( array ( 'action' => 'subset' , 'pile' => $other_pile->getTrack() , 'ts' => $this->getCurrentTimestamp() ) ) ;
	}
	
	/**
		Adds all pages from another pagepile, skipping duplicates. Metadata from the other pile for existing pages is discarded.
		\param $other_pile_candidate Numeris pile ID, or PagePile object
	*/
	public function union ( $other_pile_candidate ) {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$other_pile = $this->getAsPile($other_pile_candidate) ;
		$this->ensureCompatability($other_pile) ;
		$other = $other_pile->getSqliteFile() ;
		if ( !isset($other) ) throw new Exception ( "No 'other' sqlite file to subset" ) ;
		$sql = array() ;
		$sql[] = "ATTACH DATABASE '" . $sqlite->escapeString($other) . "' AS 'other'" ;
		$sql[] = "BEGIN EXCLUSIVE TRANSACTION" ;
		$sql[] = "INSERT OR IGNORE INTO main.pages (page,ns,json) SELECT page,ns,json FROM other.pages" ;
		$sql[] = "COMMIT" ;
		$sql[] = "DETACH DATABASE 'other'" ;
		$sql = implode ( '; ' , $sql ) ;
		$sqlite->exec ( $sql ) ;
		$this->addTrack ( array ( 'action' => 'union' , 'pile' => $other_pile->getTrack() , 'ts' => $this->getCurrentTimestamp() ) ) ;
	}
	

	public function getNewSqliteFile ( $user = '' , $wiki = '' ) {
		global $oauth_pagepile ;
		if ( $user == '' and $oauth_pagepile->is_authorized ) $user = $oauth_pagepile->username ;
		if ( $user == '' ) $user = 'Anonymous user' ; # Fallback
		if ( $wiki != '' ) $this->wiki = $wiki ;
		if ( get_current_user() == 'tools.pagepile' ) {
			return $this->getNewSqliteFileDirect ( $user ) ;
		} else {
			return $this->getNewSqliteFileFromApi ( $user ) ;
		}
	}
	
	public function setTrackFromUrl ( $label ) {
		$o = $this->getTrack() ;
		$o->label = $label ;
		$o->url = "https://tools.wmflabs.org" . $_SERVER['REQUEST_URI'] ;
		$this->setTrack ( $o ) ;
	}
	
	public function addTrack ( $o ) {
		$orig = $this->getTrack() ;
		$this->setTrack ( array ( $orig , $o ) ) ;
	}
	
	public function getTrack () {
		$sqlite = $this->getSqlite() ;
		$sql = "SELECT `value` FROM meta WHERE `key`='track'" ;
		$r = $sqlite->querySingle ( $sql ) ;
		if ( !isset($r) or $r == '' ) $r = '{"action":"blank"}' ;
		$j = json_decode ( $r ) ;
		return $j ;
	}

	public function setTrack ( $o ) {
		$sqlite = $this->getSqlite() ;
		$sql = "DELETE FROM meta WHERE `key`='track'; INSERT INTO meta (`key`,`value`) VALUES ('track','" . $sqlite->escapeString(json_encode($o)) . "')" ;
		$sqlite->exec ( $sql ) ;
	}
	
	/**
		Converts the current pile of Wikidata items to their corresponding pages on a wiki.
		Items without corresponding page in that wiki get removed.
		Execution will halt if the current wkiki is not wikidatawiki.
		\params $wiki The target wiki, e.g. enwiki or dewikisource.
	*/
	public function fromWikidata ( $wiki ) {
		$this_wiki = $this->getWiki() ;
		if ( $this_wiki != 'wikidatawiki' ) throw new Exception ( "Trying to convert from Wikidata, but this is $this_wiki" ) ;
		$this->loadNamespaces ( $wiki ) ;
		$this->loadNamespaces ( 'enwiki' ) ;
		$db = $this->openWikidataDB() ;
		$wiki = $db->real_escape_string ( $wiki ) ;
		$sqlite = $this->getSqlite() ;

		$this->getAsBatches ( function ( $batch , $pp ) use ($wiki,&$db,&$sqlite) {
			$remove_ids = array() ;
			$q = array() ;
			foreach ( $batch AS $b ) {
				$numq = preg_replace ( '/\D/' , '' , $b->page ) ;
				if ( $b->ns == 0 && $numq != '' ) $q[$numq] = $b->id ;
				else $remove_ids[] = $b->id ;
			}
		
		
			$this->beginTransaction() ;
			$sql = "SELECT ips_site_page,ips_item_id FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_item_id IN (" . implode ( ',' , array_keys($q) ) . ")" ;
			if(!$result2 = $db->query($sql)) throw new Exception('2 There was an error running the query [' . $db->error . ']');
			while($o = $result2->fetch_object()){
				$a = $this->splitFullPageName ( $o->ips_site_page ) ;
				$sql = "UPDATE pages SET page='" . $sqlite->escapeString($a[0]) . "',ns={$a[1]} WHERE id=" . $q[$o->ips_item_id] ;
				$sqlite->exec ( $sql ) ;
				unset ( $q[$o->ips_item_id] ) ;
			}
			
			foreach ( $q AS $r ) $remove_ids[] = $r ;
			if ( count ( $remove_ids ) > 0 ) {
				$sql = "DELETE FROM pages WHERE id IN (" . implode(',',$remove_ids) . ")" ;
				$sqlite->exec ( $sql ) ;
			}
			$this->commitTransaction() ;
			
			
		} , $this->defaultBatchSize ) ;


		$sql = "DELETE FROM meta WHERE `key`='wiki'; INSERT INTO meta (`key`,`value`) VALUES ('wiki','$wiki')" ;
		$sqlite->exec ( $sql ) ;
		$this->wiki = $wiki ;
		$this->addTrack ( array ( 'action' => 'from_wikidata' , 'label' => "Switching from Wikidata to matching pages on $wiki" , 'ts' => $this->getCurrentTimestamp() ) ) ;
	}
	
	/**
		Converts the current pile of pages to their corresponding Wikidata items.
		Pages without Wikidata item get removed.
		Noting will happen if the wiki is already wikidatawiki.
	*/
	public function toWikidata () {
		$wiki = $this->getWiki() ;
		if ( $wiki == 'wikidatawiki' ) return ;
		$this->loadNamespaces ( $wiki ) ;
		$this->loadNamespaces ( 'enwiki' ) ;
		$db = $this->openWikidataDB() ;
		$wiki = $db->real_escape_string ( $wiki ) ;
		$sqlite = $this->getSqlite() ;
		
		$this->getAsBatches ( function ( $batch , $pp ) use ($wiki,&$db,&$sqlite) {
			$titles = array() ;
			$title2id = array() ;
			foreach ( $batch AS $o ) {
				$title = $pp->getFullPageTitle ( $o->page , $o->ns ) ;
				$title = str_replace ( '_' , ' ' , $title ) ;
				$titles[] = $db->real_escape_string ( $title ) ;
				$title2id[$title] = $o->id ;
			}
			$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page IN ('" . implode ( "','" , $titles ) . "')" ;
			if(!$result2 = $db->query($sql)) throw new Exception('4 There was an error running the query [' . $db->error . ']');
			$this->beginTransaction() ;
			while($o = $result2->fetch_object()){
				$sqlite->exec ( "UPDATE pages SET page='Q{$o->ips_item_id}',ns=0 WHERE id=" . $title2id[$o->ips_site_page] ) ;
				unset ( $title2id[$o->ips_site_page] ) ;
			}
			if ( count ( $title2id ) > 0 ) {
				$sqlite->exec ( "DELETE FROM pages WHERE id IN (" . implode(',',$title2id) . ")" ) ;
			}
			$this->commitTransaction() ;
			
		} , $this->defaultBatchSize ) ;

		// Switch meta to wikidata
		$sql = "DELETE FROM meta WHERE `key`='wiki'; INSERT INTO meta (`key`,`value`) VALUES ('wiki','wikidatawiki')" ;
		$sqlite->exec ( $sql ) ;
		$this->wiki = 'wikidatawiki' ;
		$this->addTrack ( array ( 'action' => 'to_wikidata' , 'label' => 'Switching to matching Wikidata items' , 'ts' => $this->getCurrentTimestamp() ) ) ;
	}
	
	/**
		Removes all pages that have a Wikidata item.
		\return Number of changed pages.
	*/
	public function removePagesWithWikidataItem () {
		$wiki = $this->getWiki() ;
		if ( $wiki == 'wikidatawiki' ) return ;
		$this->loadNamespaces ( $wiki ) ;
		$this->loadNamespaces ( 'enwiki' ) ;
		$db = $this->openWikidataDB() ;
		$wiki = $db->real_escape_string ( $wiki ) ;
		$sqlite = $this->getSqlite() ;
		$ret = 0 ;
		
		$this->getAsBatches ( function ( $batch , $pp ) use ($wiki,&$db,&$sqlite) {
			$titles = array() ;
			$title2id = array() ;
			foreach ( $batch AS $o ) {
				$title = $pp->getFullPageTitle ( $o->page , $o->ns ) ;
				$title = str_replace ( '_' , ' ' , $title ) ;
				$titles[] = $db->real_escape_string ( $title ) ;
				$title2id[$title] = $o->id ;
			}
			$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page IN ('" . implode ( "','" , $titles ) . "')" ;
			if(!$result2 = $db->query($sql)) throw new Exception('removePagesWithWikidataItem: There was an error running the query [' . $db->error . ']');
			$this->beginTransaction() ;
			while($o = $result2->fetch_object()){
				$sqlite->exec ( "DELETE FROM pages WHERE id=" . $title2id[$o->ips_site_page] ) ;
				$ret++ ;
			}
			$this->commitTransaction() ;
			
		} , $this->defaultBatchSize ) ;

		// Annotate
		$this->addTrack ( array ( 'action' => 'no_wikidata' , 'label' => 'Removing pages with Wikidata items' , 'ts' => $this->getCurrentTimestamp() ) ) ;
		return $ret ;
	}
	
	public function filterByNamespace ( $keep = array() , $remove = array() ) {
		$sqlite = $this->getSqlite() ;
		$ret = 0 ;
		$this->getAsBatches ( function ( $batch , $pp ) use (&$sqlite) {
			$ids = array() ;
			foreach ( $batch AS $o ) {
				if ( in_array ( $o->ns , $keep ) ) continue ;
				if ( !in_array ( $o->ns , $remove ) and count($remove) > 0 ) continue ;
				$ids[] = $o->id ;
			}
			if ( count($ids) == 0 ) return ;
			$sql = "DELETE FROM pages WHERE id IN (" . implode(',',$ids) . ")" ;
			$sqlite->exec ( $sql ) ;
		} ) ;
		
		// Annotate
		$this->addTrack ( array ( 'action' => 'filter_by_namespace' , 'label' => 'Filtering by namespace' , 'ts' => $this->getCurrentTimestamp() ) ) ;
		return $ret ;
	}
	
	/**
		Replaces pages that are redirects with their target pages.
		\return Number of changed pages.
	*/
	public function followRedirects () {
		$ret = 0 ;
		$wiki = $this->getWiki() ;
		$this->loadNamespaces ( $wiki ) ;
		$this->loadNamespaces ( 'enwiki' ) ;
		$db = $this->openWikiDB() ;
		$sqlite = $this->getSqlite() ;

		$this->getAsBatches ( function ( $batch , $pp ) use ( &$sqlite,&$db ) {
			$nspage2id = array() ;
			$nsa = array() ;
			foreach ( $batch AS $row ) {
				$nsa[$row->ns*1][] = $db->real_escape_string(str_replace(' ','_',$row->page)) ;
			}
			$sql = array() ;
			foreach ( $nsa AS $ns => $titles ) {
				$sql[] = "page_namespace=$ns AND page_title IN ('" . implode("','",$titles) . "')" ;
			}
			$sql = "SELECT * FROM page,redirect WHERE page_id=rd_from AND (" . implode('OR',$sql) . ")" ;
			if(!$result2 = $db->query($sql)) throw new Exception('followRedirects: There was an error running the query [' . $db->error . ']'."\n$sql\n");
			$this->beginTransaction() ;
			while($o = $result2->fetch_object()){
				$sql = "UPDATE pages SET page='" . $sqlite->escapeString($o->rd_title) . "',ns={$o->rd_namespace} WHERE page='" . $sqlite->escapeString($o->page_title) . "' AND ns={$o->page_namespace}" ;
				$sqlite->exec ( $sql ) ;
				$ret++ ;
			}
			$this->commitTransaction() ;
			
		} , $this->defaultBatchSize ) ;

		$this->addTrack ( array ( 'action' => 'follow_redirects' , 'label' => 'Follow redirects' , 'ts' => $this->getCurrentTimestamp() ) ) ;
		return $ret ;
	}
	
	/**
		Calls an anonymous function with an array of objects.
		Each object is a sqlite row, the array is of size $batchsize or less.
		Check out each(), which essentially does a batch size of one.
		Also, check the PHP documentation about anonymous functions, especially the "use" command!
		\param $anon Anonymous function, parameters ( $batch , $this )
		\param $batchsize Max size of a batch. Try 1000...
	*/
	public function getAsBatches ( $anon , $batchsize ) {
		$batch = array() ;
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$sql = "SELECT * FROM pages" ;
		$results = $sqlite->query ( $sql ) ;
		while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
			$batch[] = (object) $row ;
			if ( count($batch) < $batchsize ) continue ;
			$anon ( $batch , $this ) ;
			$batch = array() ;
		}
		if ( count ( $batch ) > 0 ) $anon ( $batch , $this ) ;
	}
	
	public function getAsWikidata () {
		$ret = $this ;
		if ( $this->getWiki() != 'wikidatawiki' ) {
			$ret = $this->duplicate() ;
			$ret->toWikidata() ;
		}
		return $ret ;
	}
	
	public function getConsumerHTML () {
		$classname = 'pagepile_select_consumer_options' ;
		
		$h = "<form class='form form-inline inline-form " . $classname . "'>Use this pagepile in <select>" ;
		$h .= "<option disabled selected>one of these tools</option>" ;
		$first = true ;
		foreach ( $this->consumers AS $label => $v ) {
			$url = $v[0] ;
			$h .= "<option value='" . urlencode($url) . "'" ; // ."&pagepile_enabeled=1"
			if ( $first ) {
				$first = false ;
			}
			if ( isset($v[1]) ) {
				$h .= " title='" . preg_replace ( "/'/" , '&quot;' , $v[1] ) . "'" ;
			}
			$h .= ">$label</option>" ;
		}
		$h .= "</select></form>" ;
		$h .= '<script>$(document).ready(function(){$("form.'.$classname.' select").on("change",function(e){var url = decodeURIComponent($("option:selected", this).val()).replace(/\$1/g,"'.$this->getPileID().'"); window.location.href=url; })})</script>' ; // 
		return $h ;
	}
	
	
// ********************

	protected function getArrayWithMode ( $offset , $limit , $mode ) {
		$ret = array() ;
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$sql = "SELECT * FROM pages ORDER BY id LIMIT " . ($offset*1) ;
		if ( $limit != null ) $sql .= "," . ($limit*1) ;
		$results = $sqlite->query ( $sql ) ;
		while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
			if ( $mode == 'prefixed' ) $ret[] = $this->getFullPageTitle ( $row['page'] , $row['ns'] ) ;
			else $ret[] = (object) $row ;
		}
		return $ret ;
	}

	protected function ensureCompatability($other_pile) {
		if ( $this->getWiki() != $other_pile->getWiki() ) {
			throw new Exception ( "Different wikis (" . $this->getWiki() . " / " . $other_pile->getWiki() . ")" ) ;
		}
		return ;
	}

	protected function doFilter ( $anon , $mode ) {
		$this->commitTransaction() ;
		$sqlite = $this->getSqlite() ;
		$sql = "SELECT * FROM pages" ;
		$results = $sqlite->query ( $sql ) ;
		$remove = array() ;
		while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
			$result = $anon ( (object) $row , $this ) ;
			if ( ($mode and !$result) or (!$mode and $result) ) $remove[] = $row['id'] ;
		}
		if ( count($remove) == 0 ) return ;
		$sql = "DELETE FROM pages WHERE id IN (" . implode(',',$remove) . ")" ;
		$sqlite->exec ( $sql ) ;
		$label = "Filter: removed " . count($remove) . " pages" ;
		$this->addTrack ( array ( 'action' => 'filter' , 'label' => $label , 'ts' => $this->getCurrentTimestamp() ) ) ;
	}

	protected function getAsPile ( $d ) {
		if ( is_string($d) or is_numeric($d) ) {
			return new PagePile ( $d ) ;
		} else {
			return $d ; // Is a PagePile, I hope...
		}
	}
	
	protected function getRandomFilePath () {
		$m = md5 ( 'dummy'.rand() ) ;
		$dir = $this->file_root ;
		$dir .= '/dir_' . substr($m,0,2) ;
		if ( !file_exists ( $dir ) ) mkdir ( $dir ) ;
		$dir .= '/dir_' . substr($m,2,2) ;
		if ( !file_exists ( $dir ) ) mkdir ( $dir ) ;
		return $dir ;
	}

	protected function getNewSqliteFileDirect ( $user , $register = 'yes' ) {
		$this->openDB() ;
		$file = '' ;
		while (true) {
			$filename = uniqid('pagepile', true) . '.sqlite';
			$file = $this->getRandomFilePath() . '/' . $filename ;
			if (!file_exists($file)) break;
		}
		touch ( $file ) ;
		chmod ( $file , 0766 ) ;

		if ( !file_exists($file) ) {
			throw new Exception('Could not create sqlite file '.$file) ;
		}
		
		if ( $register == 'yes' ) {
			$ts = $this->getCurrentTimestamp() ;
			$sql = "INSERT INTO piles (path,user,created,touched) VALUES ('" . $this->db->real_escape_string($file) . "','" . $this->db->real_escape_string($user) . "','$ts','$ts')" ;
			if(!$result = $this->db->query($sql)) throw new Exception('5 There was an error running the query [' . $this->db->error . ']');
			$this->pile_id = $this->db->insert_id ;
		} else {
			$this->unregistered_sqlite_file = $file ; // For auto-cleanup
		}
		
		$this->sqlite_file = $file ;
		return $file ;
	}

	protected function getNewSqliteFileFromApi ( $user ) {
		ini_set('user_agent','PagePile tool'); # Fake user agent
		$url = 'https://tools.wmflabs.org/pagepile/api.php?action=create_pile&user=' . urlencode($user) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		if ( $j->status != 'OK' ) throw new Exception ( "Cannot create new pile: " . $j->status ) ;
		$this->pile_id = $j->pile->id ;
		$this->sqlite_file = $j->pile->file ;
		return $this->sqlite_file ;
	}

	protected function mytrim ( $s ) {
		return str_replace ( ' ' , '_' , trim ( str_replace ( '_' , ' ' , $s ) ) ) ;
	}

	protected function splitFullPageName ( $fullpage ) {
		$fullpage = $this->mytrim ( $fullpage ) ;
		$parts = explode ( ':' , $fullpage , 2 ) ;
		if ( count ( $parts ) == 1 ) return array ( $fullpage , 0 ) ; // No ":", default namespace 0
		$parts[0] = $this->mytrim ( $parts[0] ) ;
		$parts[1] = $this->mytrim ( $parts[1] ) ;
		$wiki = $this->getWiki() ;
		if ( isset ( $this->namespace_cache->$wiki->name2id[$parts[0]] ) ) return array ( ucfirst($parts[1]) , $this->namespace_cache->$wiki->name2id[$parts[0]] ) ;
		if ( $wiki != 'enwiki' ) {
			$wiki = 'enwiki' ;
			if ( isset ( $this->namespace_cache->$wiki->name2id[$parts[0]] ) ) return array ( ucfirst($parts[1]) , $this->namespace_cache->$wiki->name2id[$parts[0]] ) ;
		}
		return array ( $fullpage , 0 ) ; // Not a namespace prefix
	}

	protected function initSqliteFile ( $file ) {
		if ( file_exists ( $file ) ) {
			if ( filesize ( $file ) > 0 ) throw new Exception ( "File $file exists and is not empty, cannot init!" ) ;
		}
		$this->sqlite_file = $file ;
		$sqlite = $this->getSqlite() ;
		
		// Create tables
		$result = $sqlite->exec ( 'CREATE TABLE pages (id INTEGER PRIMARY KEY,page MEDIUMTEXT,ns INTEGER,json MEDIUMTEXT)' ) ;
		if(!$result) {
			$e = $sqlite->lastErrorCode() ;
			$es = $sqlite->lastErrorMsg() ;
			throw new Exception('A There was an error running the query [' . "$e:$es" . ']');
		}
		
		$sqlite->exec ( 'CREATE TABLE meta (id INTEGER PRIMARY KEY,key MEDIUMTEXT,value MEDIUMTEXT)' ) ;
		
		// Create indices
		$sqlite->exec ( 'CREATE UNIQUE INDEX page_index ON pages (page,ns)' ) ;
		
		// Misc
		$json = json_encode ( array('action'=>'created','id'=>$this->getPileID(),'ts'=>$this->getCurrentTimestamp()) ) ;
		$sqlite->exec ( 'INSERT INTO meta (key,value) values ("wiki","' . $sqlite->escapeString($this->getWiki())  . '")' ) ;
		$sqlite->exec ( 'INSERT INTO meta (key,value) values ("track",\''.$sqlite->escapeString($json).'\')' ) ;
	}

	protected function reset () {
		$this->commitTransaction() ;
		$this->transaction_is_running = false ;
		if ( isset($this->wikidata_db) ) $this->wikidata_db->close() ;
		unset ( $this->wikidata_db ) ;
		if ( isset($this->wiki_db) ) $this->wiki_db->close() ;
		unset ( $this->wiki_db ) ;
		if ( isset($this->sqlite) ) $this->sqlite->close() ;
		unset ( $this->sqlite ) ;
		if ( isset ( $this->db ) ) $this->db->close() ;
		
		// Clean up unregistered sqlite file
		if ( isset ( $this->unregistered_sqlite_file ) ) {
			if ( file_exists ( $this->unregistered_sqlite_file ) ) unlink ( $this->unregistered_sqlite_file ) ;
			unset ( $this->unregistered_sqlite_file ) ;
		}
		
		unset ( $this->db ) ;
		unset ( $this->wiki ) ;
		unset ( $this->pile_id ) ;
	}

	protected function loadNamespaces ( $wiki ) {
		if ( isset($this->namespace_cache->$wiki) ) return ;
		$server = $this->wiki2server ( $wiki ) ;
		$url = "https://$server/w/api.php?action=query&meta=siteinfo&siprop=namespaces|namespacealiases&format=json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$this->namespace_cache->$wiki = (object) array ( 'name2id' => array() , 'id2name' => array() ) ;
		$star = '*' ;
		foreach ( $j->query->namespaces AS $v ) {
			$this->namespace_cache->$wiki->name2id[str_replace(' ','_',$v->$star)] = $v->id ;
			if ( isset($v->canonical) ) $this->namespace_cache->$wiki->id2name[$v->id] = str_replace(' ','_',$v->canonical) ;
		}
		if ( !isset($j->query->namespacealiases) ) return ;
		foreach ( $j->query->namespacealiases AS $v ) {
			$this->namespace_cache->$wiki->name2id[str_replace(' ','_',$v->$star)] = $v->id ;
		}
	}
	
	public function getLanguageProject ( $wiki = '' ) {
		if ( $wiki == '' ) $wiki = $this->getWiki() ;
		if ( $wiki == 'wikidatawiki' ) return array ( 'wikidata' , 'wikidata' ) ;
		if ( $wiki == 'commonswiki' ) return array ( 'commons' , 'wikimedia' ) ;
		if ( !preg_match ( '/^(.+)(wik.+)$/' , $wiki , $m ) ) throw new Exception ( "Cannot parse wiki $wiki" ) ;
		$language = $m[1] ;
		$project = $m[2] ;
		if ( $project == 'wiki' ) $project = 'wikipedia' ;
		return array ( $language , $project ) ;
	}
	
	protected function wiki2server ( $wiki = '' ) {
		if ( $wiki == '' ) $wiki = $this->getWiki() ;
		if ( $wiki == 'wikidatawiki' ) return 'www.wikidata.org' ;
		if ( $wiki == 'commonswiki' ) return 'commons.wikimedia.org' ;
		if ( !preg_match ( '/^(.+)(wik.+)$/' , $wiki , $m ) ) throw new Exception ( "Cannot parse wiki $wiki" ) ;
		$language = $m[1] ;
		$project = $m[2] ;
		if ( $project == 'wiki' ) $project = 'wikipedia' ;
		return "$language.$project.org" ;
	}
	
	protected function openDB () {
		if ( isset($this->db) ) return ;
		$ini_file = $_SERVER["DOCUMENT_ROOT"] . '/../replica.my.cnf' ;
		if ( preg_match ( '/^tools\.(.+)$/' , get_current_user() , $m ) ) {
			$ini_file = "/data/project/{$m[1]}/replica.my.cnf" ;
		}
		$ini = parse_ini_file ( $ini_file ) ;
		$this->db = new mysqli($this->mysql_server, $ini['user'], $ini['password'], $this->dbname);
		if($this->db->connect_errno > 0) {
			$this->error = 'Unable to connect to database [' . $this->db->connect_error . ']';
			throw new Exception ( $this->error ) ;
			return false ;
		}
	}

	protected function openWikidataDB () {
		if ( isset($this->wikidata_db) ) return $this->wikidata_db ;
		$ini_file = $_SERVER["DOCUMENT_ROOT"] . '/../replica.my.cnf' ;
		if ( preg_match ( '/^tools\.(.+)$/' , get_current_user() , $m ) ) {
			$ini_file = "/data/project/{$m[1]}/replica.my.cnf" ;
		}
		$ini = parse_ini_file ( $ini_file ) ;
		$db = new mysqli('wikidatawiki.labsdb', $ini['user'], $ini['password'], 'wikidatawiki_p');
		if($db->connect_errno > 0) {
			$this->error = 'Unable to connect to database [' . $db->connect_error . ']';
			throw new Exception ( $this->error ) ;
			return false ;
		}
		$this->wikidata_db = $db ;
		return $db ;
	}

	protected function openWikiDB () {
		$wiki = $this->getWiki() ;
#		if ( isset($this->wiki_db) ) return $this->wiki_db ;
		$ini_file = $_SERVER["DOCUMENT_ROOT"] . '/../replica.my.cnf' ;
		if ( preg_match ( '/^tools\.(.+)$/' , get_current_user() , $m ) ) {
			$ini_file = "/data/project/{$m[1]}/replica.my.cnf" ;
		}
		$ini = parse_ini_file ( $ini_file ) ;
		$db = new mysqli($wiki.'.labsdb', $ini['user'], $ini['password'], $wiki.'_p');
		if($db->connect_errno > 0) {
			$this->error = 'Unable to connect to database [' . $db->connect_error . ']';
			throw new Exception ( $this->error ) ;
			return false ;
		}
		$this->wiki_db = $db ;
		return $db ;
	}

	protected function lp2wiki ( $language , $project ) {
		$ret = $language ;
		if ( $language == 'commons' ) $ret = 'commonswiki' ;
		elseif ( $language == 'wikidata' || $project == 'wikidata' ) $ret = 'wikidatawiki' ;
		elseif ( $language == 'mediawiki' || $project == 'mediawiki' ) $ret = 'mediawikiwiki' ;
		elseif ( $language == 'meta' && $project == 'wikimedia' ) $ret = 'metawiki' ;
		elseif ( $project == 'wikipedia' ) $ret .= 'wiki' ;
		elseif ( $project == 'wikispecies' ) $ret = 'specieswiki' ;
		elseif ( $project == 'wikisource' ) $ret .= 'wikisource' ;
		elseif ( $project == 'wiktionary' ) $ret .= 'wiktionary' ;
		elseif ( $project == 'wikibooks' ) $ret .= 'wikibooks' ;
		elseif ( $project == 'wikinews' ) $ret .= 'wikinews' ;
		elseif ( $project == 'wikiversity' ) $ret .= 'wikiversity' ;
		elseif ( $project == 'wikivoyage' ) $ret .= 'wikivoyage' ;
		elseif ( $project == 'wikiquote' ) $ret .= 'wikiquote' ;
		elseif ( $language == 'meta' ) $ret .= 'metawiki' ;
		else if ( $project == 'wikimedia' ) $ret .= $language.$project ;
		else throw new Exception ( "Cannot construct wiki name for $language.$project - aborting." ) ;
		return $ret ;
	}
	
}

?>
