var filters = [
] ;

var templates = {
	import : { locked:true, label:'l_start_with' , desc:'desc_import' , params:[{name:'pile',type:'number',placeholder:'pagepile_id'}] } ,
	union : {
		label : 'l_union' ,
		desc : 'desc_union' ,
		params : [ {name:'pile',type:'number',label:'l_pile2merge',placeholder:'pagepile_id'} ]
	} ,
	subset : {
		label : 'l_subset' ,
		desc : 'desc_subset' ,
		params : [ {name:'pile',type:'number',label:'l_pile2subset',placeholder:'pagepile_id'} ]
	} ,
	exclusive : {
		label : 'l_exclusive' ,
		desc : 'desc_exclusive' ,
		params : [ {name:'pile',type:'number',label:'l_pile2subset',placeholder:'pagepile_id'} ]
	} ,
	follow_redirects : {
		label : 'l_follow_redirects' ,
		desc : 'desc_follow_redirects' ,
		params : []
	} ,
	filter_namespace : {
		label : 'l_filter_namespace' ,
		desc : 'desc_filter_namespace' ,
		params : [
			{name:'keep',type:'text',label:'l_keep_pages_ns',placeholder:'ph_ns',optional:true } ,
			{name:'remove',type:'text',label:'l_remove_pages_ns',placeholder:'ph_ns',optional:true }
		]
	} ,
	to_wikidata : {
		label : 'l_to_wikidata' ,
		desc : 'desc_to_wikidata' ,
		params : []
	} ,
	from_wikidata : {
		label : 'l_from_wikidata' ,
		desc : 'desc_from_wikidata' ,
		params : [ {name:'wiki',type:'text',label:'l_target_wiki',placeholder:'ph_wiki_example'} ]
	} ,
	no_wikidata : {
		label : 'l_no_wikidata' ,
		desc : 'desc_no_wikidata' ,
		params : []
	} ,
	random_subset : {
		label : 'l_random_subset' ,
		desc : 'desc_random_subset' ,
		params : [ {name:'size',type:'number',label:'l_num_pages_keep'} ]
	} ,
} ;

function escattr ( s ) {
	return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#x27;').replace(/\//g,'&#x2F;') ;
}

function fixFilterTranslations () {
	$.each ( filters , function ( kf , f ) {
		if ( typeof f.type == 'undefined' ) return ;
		if ( typeof templates[f.type] == 'undefined' ) return ;
		f.label = templates[f.type].label ;
		f.desc = templates[f.type].desc ;
		if ( typeof f.params == 'undefined' ) return ;
		if ( typeof templates[f.type].params == 'undefined' ) return ;
		$.each ( f.params , function ( kp , p ) {
			if ( typeof templates[f.type].params[kp] == 'undefined' ) return ;
			p.label = templates[f.type].params[kp].label ;
			p.placeholder = templates[f.type].params[kp].placeholder ;
		} ) ;
	} ) ;
}

function renderAppendFilter ( filter ) {
	var h = "<div class='filter_wrapper' filter_id='" + filter.num + "'>" ;
	
	
	h += "<div>" ;
	h += "<div class='filter_part'>#" + (filter.num+1) + "</div>" ;
	h += "<div class='filter_part'><b tt='" + filter.label + "'></b></div>" ;
	
	$.each ( (filter.params||[]) , function ( k , v ) {
		h += "<div class='filter_part'>" ;
		if ( typeof v.label != 'undefined' ) h += "<span tt='"+v.label+"'></span>: " ;
		h += "<input type='" + v.type + "' param_num='" + k + "' filter_num='" + filter.num + "' class='filter_param'" ;
		if ( typeof v.placeholder != 'undefined' ) h += " tt_placeholder='" + v.placeholder + "'" ;
		if ( typeof v.value != 'undefined' ) h += " value='" + v.value + "'" ;
		if ( v.optional ) h += " optional=1" ;
		h += " /></div>" ;
	} ) ;

	h += "<div class='pull-right' style='font-size:9pt'>" ;
	
	h += '<div class="btn-group">' ;
	h += '<a class="btn btn-info dropdown-toggle btn-small" data-toggle="dropdown" href="#"><span tt="add_after"></span> <span class="caret"></span></a>' ;
	h += '<ul class="dropdown-menu">' ;
	$.each ( templates , function ( k , v ) {
		if ( v.locked ) return ;
		h += "<li>" ;
		h += "<a href='#' class='add_after_filter' filter_name='" + k + "' filter_num='" + filter.num + "' data-toggle='tooltip' tt_title='" + (v.desc||'') + "' tt='" + v.label + "'></a>" ;
		h += "</li>" ;
	} ) ;
	h += "</ul></div>" ;
	
	if ( !filter.locked ) {
		h += "<div>" ;
		h += "<button class='btn btn-danger btn-small delete_filter' filter_num='" + filter.num + "' tt_title='delete_step' tt='delete'></button>" ;
		if ( filter.num > 0 && !filters[filter.num-1].locked ) {
			h += " <button class='btn  btn-small move_filter_up' filter_num='" + filter.num + "' tt_title='move_up'>&uArr;</button>" ;
		}
		if ( filter.num+1 < filters.length ) {
			h += " <button class='btn  btn-small move_filter_down' filter_num='" + filter.num + "' tt_title='move_down'>&dArr;</button>" ;
		}
		h += "</div>" ;
	}
	h += "</div>" ;
	
	h += "</div>" ;

	if ( typeof filter.desc != 'undefined' ) h += "<div class='filter_desc' tt='" + filter.desc + "'></div>" ;
	h += "</div>" ;
	$('#filters').append ( h ) ;
	tt.updateInterface ( $('#filters') ) ;
}

var undo = [] ;

function cacheUndo(action) {
	var o = $.extend ( {} , { filters:$.extend([],filters) , action:action } ) ;
	undo.push ( o ) ;
	$('#undo').show() ;
}

function runFilters () {
	$('#run_output').html('<i tt="running_filters"></i>') ;
	tt.updateInterface ( $('#run_output') ) ;
	$.get ( './api.php' , {
		action : 'run_filters' ,
		filters : JSON.stringify ( filters )
	} , function ( d ) {

		var h = "<h3 tt='results'></h3><table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th>#</th><th tt='action'>Action</th><th tt='toolname'></th><th tt='pages'></th></tr></thead>" ;
		h += "<tbody>" + d.log.join("") + "</tbody></table>" ;
		$('#run_output').html(h) ;
		tt.updateInterface ( $('#run_output') ) ;
	} , 'json' ) ;
}

function checkFilterParams () {
	var bad = false ;
	$('input.filter_param').removeClass ( 'badParameter' ) ;
	$('input.filter_param').each ( function () {
		var o = $(this) ;
		var type = o.attr('type') ;
		if ( type == 'text' || type == 'number' ) {
			if ( o.val() == '' && o.attr('optional') != '1' ) {
				o.addClass ( 'badParameter' ) ;
				bad = true ;
			}
		}
	} ) ;
	
	if ( bad ) {
		$('#run_output').html('<i style="color:#FF4848" tt="please_fill_missing_params"></i>') ;
		tt.updateInterface ( $('#run_output') ) ;
	} else {
		runFilters() ;
	}
}

function renderFilters () {
	$('#filters').html('') ;
	$.each ( filters , function ( num , filter ) {
		filter.num = num ;
		renderAppendFilter ( filter ) ;
	} ) ;
	
	var h = "<div style='text-align:right;margin-top:20px;padding-top:10px;border-top:1px solid #EEE'>" ;
	h += "<button class='btn btn-info' style='display:none' id='undo' tt='undo'></button>" ;
	h += " <button class='btn btn-primary' id='run' tt='run_filters'></button>" ;
	h += "</div>" ;
	
	h += "<div id='run_output'></div>" ;
	
	h += "<div style='margin-top:20px;padding-top:10px;border-top:1px dotted #DDD'>" ;
	h += "<p tt='pastebin_lead'></p>" ;
	h += "<div style='display:inline-block'>" ;
	h += "<form id='store_pastebin' class='form form-inline inline-form' method='post' target='_blank' action='https://tools.wmflabs.org/paste/api/create'>" ;
	h += "<button class='btn btn-success' id='store' tt='store_filters'></button>" ;
	h += "<input type='hidden' name='title' tt_value='pagepile_filters' />" ;
	h += "<input type='hidden' name='text' value='' /></form>" ;
	h += "</div>" ;
	h += " <i>or</i><br/><div style='display:inline-block;margin-top:10px'>" ;
	h += "<form id='load_pastebin' class='form form-inline inline-form'>" ;
	h += "<input type='text' id='pastebin_id' value='' tt_placeholder='pastebin_id'/> <input type='submit' class='btn btn-success' id='load' tt_value='load_from_pastebin' /></form>" ;
	h += "</div>" ;
	h += "<br/><a href='/paste/' target='_blank' tt='check_out_pastebin'></a>" ;
	h += "</div>" ;

	$('#filters').append ( h ) ;
	tt.updateInterface ( $('#filters') ) ;
	
	$('#run').click ( function () {
		checkFilterParams() ;
		return false ;
	} ) ;
	
	$('#filters input.filter_param').keyup ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		var param_num = o.attr('param_num')*1 ;
		filters[filter_num].params[param_num].value = o.val() ;
		o.removeClass ( 'badParameter' ) ;
	} ) ;
	
	$('#filters a.add_after_filter').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		var filter_name = o.attr('filter_name') ;
		cacheUndo('add filter') ;
		insertNewFilter ( filter_name , filter_num+1 ) ;
		renderFilters() ;
		return false ;
	} ) ;
	
	$('#filters button.delete_filter').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		cacheUndo('delete filter') ;
		filters.splice ( filter_num , 1 ) ;
		renderFilters() ;
	} ) ;
	
	$('#filters button.move_filter_up').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		cacheUndo('move filter up') ;
		var tmp = filters[filter_num-1] ;
		filters[filter_num-1] = filters[filter_num] ;
		filters[filter_num] = tmp ;
		renderFilters() ;
	} ) ;
	
	$('#filters button.move_filter_down').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		cacheUndo('move filter down') ;
		var tmp = filters[filter_num+1] ;
		filters[filter_num+1] = filters[filter_num] ;
		filters[filter_num] = tmp ;
		renderFilters() ;
	} ) ;
	
	$('#load_pastebin').submit ( function () {
		var id = $('#pastebin_id').val() ;
		var url = "https://tools.wmflabs.org/paste/view/raw/" + id ;
		$.get ( url , function ( d ) {
			d = JSON.parse ( d ) ;
			cacheUndo('load filter set') ;
			filters = d ;
			fixFilterTranslations() ;
			renderFilters() ;
		} , 'text' ) ;
		return false ;
	} ) ;
	
	$('#undo').click ( function () {
		var o = undo.pop() ;
		filters = o.filters ;
		renderFilters() ;
	} ) ;
	
	if ( undo.length > 0 ) {
		$('#undo').text("Undo "+undo[undo.length-1].action).show() ;
	}

	$('#store_pastebin input[name="text"]').val ( JSON.stringify ( filters ) ) ;
	
}

function insertNewFilter ( filter_name , position ) {
	var filter = $.extend ( {} , templates[filter_name] ) ;
	filter.type = filter_name ;
	filters.splice ( position, 0 , filter ) ;
}

function godot () {
	if ( typeof tt == 'undefined' ) {
		setTimeout ( godot , 10 ) ;
		return ;
	}
	insertNewFilter ( 'import' , 0 ) ;
	if ( start_pile != 0 ) filters[0].params[0].value = start_pile ;
	renderFilters() ;
}

$(document).ready ( godot ) ;